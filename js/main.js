var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

var x = 0;
var dx = 2;
var y = 0;
var dy = 1;
var canvasWidth = canvas.width;
var canvasHeight = canvas.height;
var count = 1;
var pauseAnimation = '';

function animate() {
  pauseAnimation = requestAnimationFrame(animate);
  c.clearRect(0, 0, canvasWidth, canvasHeight);
  c.fillStyle = '#ff0000';
  c.beginPath();
  var obj = recur(x, y, dx, dy, canvasWidth, canvasHeight, count);
  c.fillRect(obj.x, obj.y, 30, 15);

}

var recur = function(x, y, dx, dy, canvasWidth, canvasHeight, count) {

  if(this.x >= canvasWidth - (30 * count)) {
    this.x = canvasWidth - (30 * count);

    if(this.y >= canvasHeight - (15 * count)) {
      this.y = canvasHeight - (15 * count);
      this.dx = -dx;
      this.x += this.dx;
    } else {
      this.y += dy;
    }

  } else {
    this.x += dx;

    if(this.x <= (30 * (count - 1))) {
      this.x = (30 * (count - 1));
      this.y += -dy;

      if(this.y <= (15 * count)) {
        count++;
        this.dx = 2;
        var recursiveObj = recur(this.x, this.y, this.dx, this.dy, canvasWidth, canvasHeight, count);
        this.x = recursiveObj.x;
        this.y = recursiveObj.y;
        this.count = count;
        this.dx = recursiveObj.dx;
        this.dy = recursiveObj.dy;
      }
    }

  }

  return this;
};

var playButton = document.getElementById('play');
playButton.addEventListener('click', function() {
  requestAnimationFrame(animate);
});

var pauseButton = document.getElementById('pause');
pauseButton.addEventListener('click', function() {
  cancelAnimationFrame(pauseAnimation);
});

animate();
