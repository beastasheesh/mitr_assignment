if(this.x >= canvasWidth - (30 * count)) {
  this.x = canvasWidth - (30 * count);

  if(this.y >= canvasHeight - (15 * count)) {
    this.y = canvasHeight - (15 * count);
    this.dx = -dx;
    this.x += this.dx;
  } else {
    this.y += dy;
  }

} else {
  this.x += dx;

  if(this.x <= (30 * (count - 1))) {
    this.x = (30 * (count - 1));
    this.y += -dy;

    if(this.y <= (15 * count)) {
      count++;
      this.dx = 2;
      var recursiveObj = recur(this.x, this.y, this.dx, this.dy, canvasWidth, canvasHeight, count);
      this.x = recursiveObj.x;
      this.y = recursiveObj.y;
      this.count = count;
      this.dx = recursiveObj.dx;
      this.dy = recursiveObj.dy;
    }
  }

}

return this;
